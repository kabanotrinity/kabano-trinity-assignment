#include <stdio.h>
#include <math.h>

// Function to calculate the discriminant
double calculate_discriminant(double a, double b, double c) {
    return (b * b) - (4 * a * c);
}

int main() {
    double a, b, c;
    double discriminant;

    // Prompt user to enter coefficients
    printf("Enter the coefficients (a, b, c): ");
    scanf("%lf %lf %lf", &a, &b, &c);

    // Validate input
    if (a == 0) {
        printf("Error: Coefficient 'a' cannot be zero.\n");
        return 1;
    }

    // Calculate discriminant
    discriminant = calculate_discriminant(a, b, c);

    // Determine roots
    if (discriminant > 0) {
        // Real and distinct roots
        double root1 = (-b + sqrt(discriminant)) / (2 * a);
        double root2 = (-b - sqrt(discriminant)) / (2 * a);
        printf("Roots are real and distinct: %.2lf and %.2lf\n", root1, root2);
    } else if (discriminant == 0) {
        // Real and equal roots
        double root = -b / (2 * a);
        printf("Roots are real and equal: %.2lf\n", root);
    } else {
        // Complex roots
        printf("Roots are complex\n");
    }

    return 0;
}
//the result of  whether its complex root or not
